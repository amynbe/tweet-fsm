//=======================================================================
// Copyright 2001 University of Notre Dame.
// Author: Jeremy G. Siek
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//=======================================================================

/*
  Sample output:

  G0:
  0 --> 1
  1 --> 2 3
  2 --> 5
  3 -->
  4 --> 1 5
  5 --> 3
  0(0,1) 1(1,2) 2(1,3) 6(2,5) 3(4,1) 4(4,5) 5(5,3)

  G1:
  2 --> 5
  4 --> 5
  5 -->
  6(2,5) 4(4,5)

  G2:
  0 --> 1
  1 -->
  0(0,1)

 */

#include <boost/config.hpp>
#include <iostream>
#include <boost/graph/subgraph.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>

int main(int,char*[])
{
  using namespace boost;
  typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
  typedef subgraph< adjacency_list<vecS, vecS, directedS,
    property<vertex_color_t, int>, property<edge_index_t, int> > > Graph;

  Graph* G[3];
  const int N = 10;
  Graph G0(N);
  enum { A, B, C, D, E, F};     // for conveniently referring to vertices in G0

  Graph& G1 = G0.create_subgraph();
  Graph& G2 = G0.create_subgraph();

  G[0] = &G0;
  G[1] = &G1;
  G[2] = &G2;

  enum { A1, B1, C1 };          // for conveniently referring to vertices in G1
  enum { A2, B2 };              // for conveniently referring to vertices in G2

  add_vertex(C, G1); // global vertex C becomes local A1 for G1
  add_vertex(E, G1); // global vertex E becomes local B1 for G1
  add_vertex(F, G1); // global vertex F becomes local C1 for G1

  add_vertex(A, G2); // global vertex A becomes local A1 for G2
  add_vertex(B, G2); // global vertex B becomes local B1 for G2

  Graph::edge_descriptor Ed[7];
  Ed[0] = add_edge(A, B, G0).first;
  Ed[1] = add_edge(B, C, G0).first;
  Ed[2] = add_edge(B, D, G0).first;
  Ed[3] = add_edge(E, B, G0).first;
  Ed[4] = add_edge(E, F, G0).first;
  Ed[5] = add_edge(F, D, G0).first;

  Ed[6] = add_edge(A1, C1, G1).first; // (A1,C1) is subgraph G1 local indices for (C,F).

  std::cout << "G0:" << std::endl;
  print_graph(G0, get(vertex_index, G0));
  print_edges2(G0, get(vertex_index, G0), get(edge_index, G0));
  std::cout << std::endl;

  Graph::children_iterator ci, ci_end;
  int num = 1;
  for (boost::tie(ci, ci_end) = G0.children(); ci != ci_end; ++ci) {
    std::cout << "G" << num++ << ":" << std::endl;
    print_graph(*ci, get(vertex_index, *ci));
    print_edges2(*ci, get(vertex_index, *ci), get(edge_index, *ci));
    std::cout << std::endl;
  }

  for (unsigned int in = 0; in<3; ++in)
  {
      std::pair<Graph::vertex_descriptor, bool> p1 = G[in]->find_vertex(2);
      std::pair<Graph::vertex_descriptor, bool> p2 = G[in]->find_vertex(4);
      if (!p1.second || !p2.second){continue;}
      Graph::vertex_descriptor s = p1.first;
      Graph::vertex_descriptor t = p2.first;
      bool exists = boost::edge(s, t, *G[in]).second;

      std::cout << "Edge (" << s << ", " << t << ") exists in G" << in << "? " << exists << std::endl;
//      for (unsigned int e = 0; e<3; ++e)
//      {
//          Graph::vertex_descriptor s = boost::source(Ed[e], *G[0]);
//          Graph::vertex_descriptor t = boost::target(Ed[e], *G[0]);
//          bool exists = boost::edge(s, t, *G[in]).second;
//          std::cout << "Edge (" << s << ", " << t << ") exists in G" << in << "? " << exists << std::endl;
//      }
      std::cout << std::endl;
  }
  std::cout << std::endl;
  for (unsigned int in = 0; in<3; ++in)
  {
      for (unsigned int v = 0; v<N; ++v)
      {
          bool vexists = G[in]->find_vertex(v).second;
          std::cout << "Vertex " << v << " exists in G" << in << "? " << vexists << std::endl;
      }
      std::cout << std::endl;
  }

  std::list<Graph> graphlist;
  graphlist.push_back(G0);
  graphlist.push_back(G1);
  graphlist.push_back(G2);
  std::cout<< "Graphlist: " << boost::num_edges(G0) << std::endl;
  std::cout<< "Graphlist: " << boost::num_edges(G1) << std::endl;
  std::cout<< "Graphlist: " << boost::num_edges(G2) << std::endl;


  for (std::list<Graph>::iterator it = graphlist.begin(); it != graphlist.end(); ++it)
    std::cout<< "Graphlist: " << boost::num_edges(*it) << std::endl;

  return 0;
}
