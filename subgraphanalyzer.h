#ifndef SUBGRAPHANALYZER_H
#define SUBGRAPHANALYZER_H

#include "subgraph.h"
#include <list>
#include <valarray>
class SubgraphAnalyzer
{
public:
    SubgraphAnalyzer(const std::list<std::list<Subgraph> > &);
    void computeRegular();

private:
    static double normalized_std_deviation(std::valarray<double> &intervals);
    std::list<std::list<Subgraph> > subgraphs;
};

#endif // SUBGRAPHANALYZER_H
