#include "subgraphanalyzer.h"
#include <list>
#include <valarray>
#include "frequentsubgraphminer.h"

#define SQUARE(x) ((x)*(x))

SubgraphAnalyzer::SubgraphAnalyzer(const std::list<std::list<Subgraph> > &subgraphs)
    : subgraphs(subgraphs)
{}

/**
 * Associate each subgraph with a boolean value which is true if the subgraph is stagnant
 *
 **/
void SubgraphAnalyzer::computeRegular()
{
    //for each list of subgraphs
    for (std::list<std::list<Subgraph> >::iterator tableIter = subgraphs.begin(); tableIter != subgraphs.end(); ++tableIter)
    {
        //for each subgraph
        for (std::list<Subgraph>::iterator subIter = tableIter->begin(); subIter != tableIter->end(); ++subIter)
        {
            if (subIter->support >= FrequentSubgraphMiner::MIN_SUPPORT)
            {
                //calculate rate of constness (--> low dispersion of intervals durations between apparitions)
                //first, calculate intervals
                std::valarray<uint> dates(subIter->timeRanges.size());
                std::valarray<double> intervals(subIter->timeRanges.size()-1);
                dates[0] = subIter->timeRanges.front().toTime_t();
                uint i=1;
                for(std::list<QDateTime>::iterator tIter = ++subIter->timeRanges.begin(); tIter != subIter->timeRanges.end(); ++i, ++tIter)
                {
                    dates[i] = tIter->toTime_t();
                    intervals[i-1] = dates[i] - dates[i-1];
                }
                double regularity = SubgraphAnalyzer::normalized_std_deviation(intervals);
                std::cout << "sub " << (*subIter) << ":\tregularity= " << regularity << std::endl;
                std::cout  << ((Subgraph)*subIter).timeRangesStr() ;
                //std::copy(intervals.begin(), intervals.end(), std::ostream_iterator<uint>(std::cout, " "));
                std::copy(&intervals[0], &intervals[intervals.size()], std::ostream_iterator<double>(std::cout, "\n"));
                std::cout<<std::endl<< std::endl;
            }
        }
    }
}

double std_deviation(std::valarray<double>& values)
{
    double mean = values.sum() / values.size();
    std::valarray<double> deviations(values-mean);
    double variance = SQUARE(deviations).sum() / values.size();
    return std::sqrt(variance);
}

double SubgraphAnalyzer::normalized_std_deviation(std::valarray<double>& intervals)
{
    const uint& MIN_INTERVAL = 0;
    valarray<double> max_dev_intervals(MIN_INTERVAL, intervals.size());
    max_dev_intervals[0] = intervals.sum() - ((max_dev_intervals.size()-1) * MIN_INTERVAL);

    std::cout<< "std_deviation=" << std_deviation(intervals) << std::endl;
    std::cout<< "max_deviation=" << std_deviation(max_dev_intervals) << std::endl;
    if (std_deviation(intervals) > std_deviation(max_dev_intervals))
    {
        std::cout << "std_dev greater than max_dev\n";
        std::copy(&max_dev_intervals[0], &max_dev_intervals[max_dev_intervals.size()],
                  std::ostream_iterator<double>(std::cout, "\n"));
    }
    return 1-(std_deviation(intervals) / std_deviation(max_dev_intervals));
}

