#ifndef GRAPH_LOADER_H
#define GRAPH_LOADER_H

#include <QDate>

#include "twgraph.h"
#include <boost/graph/adjacency_list.hpp>

class GraphLoader
{
public:
    GraphLoader(QString hostname="localhost", QString user="root", QString password="0000", int port=3306, QString database="twitter");
    GraphLoader(unsigned int);
    std::list<Twgraph> &getGraph();
private:
    void addGraph(std::list<QPair<QPair<QString, QString>, QDateTime> > &mentionList);

    std::list<Twgraph> g;
};

#endif // GRAPH_LOADER_H
