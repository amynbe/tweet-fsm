#ifndef FREQUENTSUBGRAPHMINER_H
#define FREQUENTSUBGRAPHMINER_H
#include <list>
#include "graphloader.h"
#include "twgraph.h"
#include "subgraph.h"

class FrequentSubgraphMiner
{
public:
    static const unsigned int MIN_SUPPORT = 3;
    FrequentSubgraphMiner(std::list<Twgraph>& dataset) : dataset(dataset) {}
    std::list<std::list<Subgraph> > &findFrequentSubgraphs();
private:
    std::list<Twgraph>& dataset;
    std::list<std::list<Subgraph> > subgraphTables;
};

#endif // FREQUENTSUBGRAPHMINER_H
