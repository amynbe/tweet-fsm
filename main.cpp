#include <QtCore/QCoreApplication>
#include <QStringList>
#include <QtGui/QMessageBox>
#include <QDebug>
#include <iostream>
#include "graphloader.h"
#include "frequentsubgraphminer.h"
#include "subgraphanalyzer.h"

void display(const std::list<std::list<Subgraph> > &l)
{
    unsigned int cnt=1;
    for (std::list<std::list<Subgraph> >::const_iterator i = l.begin(); i!=l.end(); ++i)
    {
        qDebug() << "-------------------------------------------------------------------------------";
        std::cout << ++cnt << "-Subgraphs" << std::endl;
        unsigned int subcnt = 0;
        for (std::list<Subgraph>::const_iterator j = i->begin(); j!=i->end(); ++j)
        {
            std::cout << ++subcnt << "th Subgraph = " << *j << std::endl;
        }
        std::cout << i->size() << " subs" << std::endl;
    }
    qDebug() << "END";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//#define fakedata
#ifdef fakedata
    GraphLoader g(3);
#else
    GraphLoader g(QCoreApplication::arguments().at(1));
#endif

    FrequentSubgraphMiner m(g.getGraph());
    const std::list<std::list<Subgraph> > &freqSubs = m.findFrequentSubgraphs();
    if (false) display(freqSubs);
    SubgraphAnalyzer analyzer(freqSubs);
    analyzer.computeRegular();
    return a.exec();
}
