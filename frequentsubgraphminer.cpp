#include "frequentsubgraphminer.h"
#include "graphloader.h"
#include "subgraph.h"
#include "twgraph.h"

#include <omp.h>
#include <math.h>

#include <functional>

#include <list>
#include <QDebug>

#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/foreach.hpp>

template<int Threshold>
struct lowSupport: public std::unary_function<Subgraph&, bool> {
	bool operator()(Subgraph& s) {
        return s.support < Threshold;
	}
};

struct sameSubIncrSupport: public std::binary_function<Subgraph&, Subgraph&,
		bool> {
	/** return true if two subgraphs are same and incrementt s1 support by s2 support **/
    bool operator()(Subgraph& s1, const Subgraph& s2)
    {
        if (s1 == s2)
        {
            s1.support += s2.support;
            std::copy(s2.timeRanges.begin(), s2.timeRanges.end(),
                      std::back_insert_iterator<std::list<QDateTime> >(s1.timeRanges));

            return true;
        }
        return false;
    }
};

double update(double& time)
{
    double difference = omp_get_wtime()- time;
    time = omp_get_wtime();
    int round = 100;
    while (floor(round*(difference))/round==0)
    {
        round*=10;
    }
    return floor(round*(difference))/round;
}

std::list<std::list<Subgraph> >& FrequentSubgraphMiner::findFrequentSubgraphs() {
    double time = omp_get_wtime();

    std::cout << "2-subs:" << std::endl;
	//initialize patterns with 2-subgraphs
	//iterate over all edges of all graphs
    std::list<Subgraph> two_subs;   //list of 2-subgraphs of all time ranges
    int i = 0;

    for(std::list<Twgraph>::iterator tw(dataset.begin());tw!=dataset.end();++tw)
    {
        const digraph* root = tw->getGraph();
        std::string graph_name = "T" + boost::lexical_cast<string>(i) + ".";

        int j = 0;
        edge_iterator it, end;
        //iterate over edges of the current time range
        for (boost::tie(it, end) = tw->edges(); it != end; ++it, ++j) {
            std::string name = graph_name + boost::lexical_cast<string>(j);
            //two_subs.push_back(*new Subgraph (tg.getGraph(), *it, name));
            two_subs.push_back(*new Subgraph (root, *it, tw->datetime, name));
//            std::cout << "New Subgraph: " << two_subs.back() << std::endl;
//            std::cout << "|";
        }


//        time = floor(100*(omp_get_wtime()- time))/100;
        std::cout << "[" << graph_name.c_str() << "(" << j << "):" << update(time) << "s] ";
        if (i%4==3) std::cout<<std::endl;

        ++i;
//        if (i==2) break;
	}
    std::cout<<std::endl;

	//compute support
    std::cout << "sorting: ";
    two_subs.sort();
    std::cout  << update(time) << "s\n";
    std::cout  << "support computation: ";
    two_subs.unique(sameSubIncrSupport());
    std::cout  << update(time) << "s\n";

	std::ostream_iterator<Subgraph> out_it(std::cout, "\n");
//    std::copy(two_subs.begin(), two_subs.end(), out_it);

    //prune low support edges
    std::cout  << "pruning edges with low support: \n";
    two_subs.remove_if(lowSupport<MIN_SUPPORT>());
    std::cout << update(time) << "s\n";
    std::copy(two_subs.begin(), two_subs.end(), out_it);

    subgraphTables.push_back(two_subs);
    for(unsigned int k=3; !subgraphTables.back().empty(); ++k) {

        std::cout << "-------------------------------------------------------------------------------\n";
        std::cout << "Building " << k << "-subs:\n";

        //generate k-subgraphs from k-1 subgraphs
        std::list<Subgraph> previousTable = subgraphTables.back();
        std::list<Subgraph> newTable;

        std::cout << "Combining subgraphs...\n";
        //for each subgraph in the previous (k-1) table
        for (std::list<Subgraph>::iterator it = previousTable.begin();
                it != previousTable.end(); ++it) {
			//again, for each subgraph in the previous table (cartesian product)
            for (std::list<Subgraph>::iterator it2(it); it2 != previousTable.end();
                    ++it2) {

//                std::cout << "[ " << *it << std::endl;
//                std::cout << "+ " << *it2 << " ]" << std::endl;
                if (it->operator==(*it2))
                {
//                    std::cout << "Are equal -- dropping\n";
                    continue;
                }


                //combine two graphs through the "peripheral edge" relationship
                Edge periph;
                if (((Subgraph)*it).findPeripheralEdge(periph, *it2)) //periph is in the periphery of *it
                {
                    newTable.push_back(*new Subgraph(*it, periph, *it2, it->name+"+1"));

//                    std::cout << "==> " << k << "-sub " << newTable.back() << std::endl;

                    //remove if already existing
                    if (newTable.size()>1)
                    {
                        std::list<Subgraph>::iterator end = --newTable.end();
                        std::list<Subgraph>::iterator existing = std::find(newTable.begin(), end,
                                                                           newTable.back());

                        if (existing != end) //found
                        {
                            std::cout << "Already existing, drop\n";
                            newTable.pop_back();
//                            existing->incrementSupport();
                        }
                    }
				}
			}
        }
        std::cout << update(time) << " seconds\n";
        //finished generating k-subgraphs
        //now compute support and prune

        if (newTable.empty()) break;


//        std::cout << "Before support computation\n";
//        std::copy(newTable.begin(), newTable.end(), out_it);

        std::cout << "Computing support...\n";
        //compute k-subgraphs support
        for(std::list<Subgraph>::iterator it = newTable.begin(); it != newTable.end(); ++it)
        {
            foreach(Twgraph transaction, dataset) {
                if (transaction.contains(*it)) {
                    it->support++;
				}
			}
        }
        std::cout << update(time) << "seconds";

//		std::cout << "Before low support pruning\n";
//		std::copy(newTable.begin(), newTable.end(), out_it);
//        std::cout << "Low support pruning...\n";
        newTable.remove_if(lowSupport<MIN_SUPPORT>());
//        std::cout << update(time) << " seconds\n";
		
//		std::cout << "After low support pruning\n";
        std::copy(newTable.begin(), newTable.end(), out_it);

        if (newTable.empty()) break;
		subgraphTables.push_back(newTable);

        std::cout << "-------------------------------------------------------------------------------\n";
	}

	return subgraphTables;
}
