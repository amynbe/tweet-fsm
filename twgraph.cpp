
#include "twgraph.h"
#include "graph_types.h"
#include "subgraph.h"
#include "node.h"

#include <list>
#include <QHash>

#include <boost/graph/undirected_graph.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>



Twgraph::Twgraph(const std::list<QPair<string, string> >& edges, const QDateTime &t)
    : datetime(t), g(new digraph())
{
//    std::for_each(edges.begin(), edges.end(), boost::bind(&Twgraph::addEdge, boost::ref(this), _1));
//    QPair<string, string> edge;

    for (std::list<QPair<string, string> >::const_iterator edge = edges.begin(); edge != edges.end(); ++edge)
    {
        addEdge(edge->first, edge->second);
    }
}

std::pair<edge_iterator, edge_iterator> Twgraph::edges() const
{
    return boost::edges(*g);
}

digraph::edge_descriptor Twgraph::addEdge(const QPair<QString, QString>& pair)
{
    return addEdge(pair.first.toStdString(), pair.second.toStdString());
}

digraph::edge_descriptor Twgraph::addEdge(const std::string& name1, const std::string& name2)
{
    using namespace boost;
    digraph::vertex_descriptor v1 = add_vertex(*g);
    digraph::vertex_descriptor v2 = add_vertex(*g);

//    property_map<digraph, vertex_name_t>::type vnames = get(vertex_name, g);
//    vnames[v1] = name1;
//    vnames[v2] = name2;
    (*g)[v1].name = name1;
    (*g)[v2].name = name2;

    return boost::add_edge(v1, v2, *g).first;
}

//sub::edge_descriptor Twgraph::addEdge(sub::edge_descriptor *e)
//{
//	//sub::vertex_descriptor u = boost::add_vertex(e->m_source, g);
//	//sub::vertex_descriptor v = boost::add_vertex(e->m_target, g);
//	//return boost::add_edge(u, v, g).first;

//	sub::vertex_descriptor v1 = boost::source(*e, g);
//	sub::vertex_descriptor v2 = boost::target(*e, g);

//    using namespace boost;
//    property_map<sub, vertex_name_t>::type props = get(vertex_name, g);

//	return addEdge(props[v1], props[v2]);

//}

bool Twgraph::contains(const Subgraph &s)
{
//    Edge e = std::pair<edge_iterator, edge_iterator>(boost::edges(s)).first;
//    sub::vertex_descriptor source = boost::source(e, s);
//    sub::vertex_descriptor target = boost::target(e, s);

    //follow source
    //follow target

	/*edge_iterator it; end;
    for(boost::tie(it, end) = s.edges(); it != end; ++it)
    {
        if (!contains(*it))
        {
            return false;
        }
    }*/

    digraph::vertex_iterator vit, vend;
	
	std::list<std::string> this_names;
//    boost::property_map<digraph, boost::vertex_name_t>::type names_map = boost::get(boost::vertex_name, g);
    for(boost::tie(vit, vend) = boost::vertices(*g); vit != vend; ++vit)
    {
//        this_names.push_back(names_map[*vit]);
        this_names.push_back((*g)[*vit].name);
    }

	std::list<std::string> other_names;
//    names_map = boost::get(boost::vertex_name, s.graph());
    std::list<std::string>::iterator it, end;
    end = this_names.end();
    for(boost::tie(vit, vend) = boost::vertices(*s.graph()); vit != vend; ++vit)
    {
//        other_names.push_back(names_map[*vit]);
        other_names.push_back((*s.graph())[*vit].name);
        it = std::find(this_names.begin(), end, (*s.graph())[*vit].name);
        if (it == end)
        {
            return false;
        }
    }
    return true;
	
//	this_names.sort();
//	other_names.sort();

//	std::vector<std::string> diff;
//    std::set_difference(this_names.begin(), this_names.end(), other_names.begin(), other_names.end(), std::inserter(diff, diff.end()));

//    for (std::vector<std::string>::iterator it = diff.begin(), it != diff.end, ++it)
//    {
//        std::find();
//    }
//    return diff.empty() ;
}


uint qHash(const Twgraph& tw)
{
    return qHash((const Twgraph *)(&tw));
}

bool operator== (const Twgraph& tw1, const Twgraph& tw2)
{
	return qHash(tw1) == qHash(tw2);
}

