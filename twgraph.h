#ifndef TWGRAPH_H
#define TWGRAPH_H

#include <list>
#include <QHash>
#include <QDateTime>
#include "graph_types.h"
#include "node.h"
#include "subgraph.h"


class Twgraph
{
public:
    Twgraph(const QDateTime t) : datetime(t), g(new digraph) {}
    Twgraph(const std::list<QPair<string, string> >&, const QDateTime&);

    std::pair<edge_iterator, edge_iterator> edges() const;
    bool contains(const Subgraph&);
    digraph::edge_descriptor addEdge(const QPair<QString, QString> &);
    digraph::edge_descriptor addEdge(const std::string& name1, const std::string& name2);
    //sub::edge_descriptor addEdge(sub::edge_descriptor *e);
    const digraph* getGraph() {return g;}

    const QDateTime datetime;

private:
    digraph* const g;
};

extern uint qHash(const Twgraph& tw);
extern bool operator== (const Twgraph& tw1, const Twgraph& tw2);



#endif // TWGRAPH_H
