#include "graphloader.h"
#include <ostream>
#include <QFile>
#include <QRegExp>
#include <QString>
#include <QTextStream>
#include <QIODevice>
#include <QDebug>
#include <QDir>
#include <QDate>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QTextCodec>
#include <list>

#include <boost/lexical_cast.hpp>
#include <boost/graph/adjacency_list.hpp>
#include "node.h"

#define q2c(string) string.toStdString()

//void initTimeFrame(QDateTime& timeFrame, QDateTime& t)
//{
//    QTime initialT = t.time();
//    initialT.setHMS(initialT.hour(), initialT.minute(), 0);
//    timeFrame.setDate(t.date()); // set date part to normal (evolving) date
//    timeFrame.setTime(initialT); // set time part to normal time, except seconds, set them to zero
////    qDebug() << "timeFrame" << timeFrame.time() << "<--" << t.time();
//    timeFrame = truncateSeconds(t);
//}

QDateTime truncateSeconds(QDateTime& t)
{
    return QDateTime(t.date(),
                     QTime(t.time().hour(), t.time().minute(), 0));
}

QDateTime incrementDateTime(const QDateTime& t)
{
    return t.addSecs(60);
}

/**
  * Create a Twgraph and populate it with the content of the edges in the passed list
  **/

void GraphLoader::addGraph(std::list<QPair<QPair<QString, QString>, QDateTime> >& mentionList)
{
    Twgraph tw(truncateSeconds(mentionList.front().second));
    QPair<QPair<QString, QString>, QDateTime> mention;
    foreach (mention, mentionList)
    {
        tw.addEdge(mention.first);
//        qDebug() << "adding" << mention.second.time() << "into place" << g.size();
    }
    g.push_back(tw);

    mentionList.clear();
}

GraphLoader::GraphLoader(QString hostname, QString user, QString password, int port, QString database)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName(hostname);
    db.setUserName(user);
    db.setPassword(password);
    db.setPort(port);
    db.setDatabaseName(database);
    if(db.open())
    {
        std::cout << "Connected to " << /*db << "//" <<*/ db.userName().toStdString() << ":" << db.password().toStdString() << "@" << db.hostName().toStdString() << ":" << db.port() << "/" << db.databaseName().toStdString() << std::endl;
        QSqlQuery query;//(db.database());

        const int MENTIONER_FIELD = 0;
        const int MENTIONED_FIELD = 1;
        const int DATE_FIELD = 2;
        std::list<QPair<QPair<QString, QString>, QDateTime> > mentionList;

        QString q = "SELECT * FROM twitter.materializedmentionview limit 10000";
        qDebug() << q;
        if(query.exec(q))
        {
            qDebug() << "Query executed";
            QDateTime timeFrame;
            int timeframes = 0;
            while(query.next())
            {
//                for(int x=0; x < query.record().count(); ++x)
//                {
//                    qDebug() << query.record().fieldName(x)
//                             << " = "
//                             << query.value(x);
//                }
                QString mentioner(query.value(MENTIONER_FIELD).toString());
                QString mentioned(query.value(MENTIONED_FIELD).toString());
                QDateTime t(query.value(DATE_FIELD).toDateTime());

                QPair<QString, QString> users(mentioner, mentioned);
                QPair<QPair<QString, QString>, QDateTime> mention(users, t);

                if (timeframes == 0)
                {
                    timeframes++;
//                    initTimeFrame(timeFrame, t);
                    timeFrame = truncateSeconds(t);

                }
                else if (t >= incrementDateTime(timeFrame))
                {
                    addGraph(mentionList);
                    timeframes++;
//                    initTimeFrame(timeFrame, t);
                    timeFrame = truncateSeconds(t);
                    std::cout << "| " << t.toString().toStdString() << ": " << timeframes << endl;
                }
                mentionList.push_back(mention);
//                qDebug() << i++;
            }
            addGraph(mentionList);
            qDebug();
            qDebug() << "There are " << timeframes << "minutes";
        }
        else
        {
            qDebug() << "Could not execute query";
        }
        db.close();
    }
    else
    {
        qDebug() << "La connexion a échoué, désolé";
    }
}

GraphLoader::GraphLoader(unsigned int nbGraphs)
{
    using namespace boost;
    string a = "a";// + lexical_cast<string>(3);
    string b = "b";// + lexical_cast<string>(5);
    string n5 = "c";// + lexical_cast<string>(6);
    string n6 = "d";// + lexical_cast<string>(6);
    string n7 = "e";// + lexical_cast<string>(6);

    for (unsigned int i=0; i<nbGraphs; ++i)
    {
        std::list<QPair<string, string> > tw;

        if (i==0) {
            tw.push_back(QPair<string, string>(b, a));
            tw.push_back(QPair<string, string>(b, a));
            tw.push_back(QPair<string, string>(b, a));
        }

        string n1 = "" + lexical_cast<string>(i);
        string n2 = "" + lexical_cast<string>(i+1);
        string n8 = "" + lexical_cast<string>(2*i+1);
        string n9 = "" + lexical_cast<string>(2*i+2);

        tw.push_back(QPair<string, string>(n1, n2));
        tw.push_back(QPair<string, string>(std::string("2"), a));
        tw.push_back(QPair<string, string>(n1, n8));
        tw.push_back(QPair<string, string>(n9, n8));

        tw.push_back(QPair<string, string>(a, b));
        tw.push_back(QPair<string, string>(b, n5));
        tw.push_back(QPair<string, string>(b, n6));
        tw.push_back(QPair<string, string>(b, n7));

        g.push_back(*new Twgraph(tw, QDateTime::currentDateTime().addSecs(60*i)));

        std::cout << "Twitter Graph " << i << ":";
        const digraph & s = *g.back().getGraph();
//        property_map<digraph, vertex_name_t>::type nameprops = get(vertex_name_t(), s);
        //print_graph(g, nameprops);
        //print_edges2(g, nameprops, get(edge_index, g));
//        print_edges(s, nameprops);
        print_edges(s, get(&VertexProperties::name, s));
        std::cout << std::endl;
    }
}

std::list<Twgraph> &GraphLoader::getGraph()
{
    return g;
}
