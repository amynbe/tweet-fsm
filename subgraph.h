#ifndef SUBGRAPH_H
#define SUBGRAPH_H
#include <iostream>
#include <string>

#include <QDateTime>

#include "graph_types.h"

using namespace std;

class Subgraph
{
public:
    Subgraph(const digraph* root, const Edge& singleEdge, const QDateTime firstTimeRange, const std::string name = "");
    Subgraph(const Subgraph &, const Edge& newEdge, const Subgraph &from, const string name = "");
    bool findPeripheralEdge(Edge& found, Subgraph &s1) const;
    inline const digraph* graph() const {return s;}
    std::pair<edge_iterator, edge_iterator> edges() const;

    digraph::vertex_descriptor src(const Edge &e) const;
    digraph::vertex_descriptor tgt(const Edge &e) const;

    const std::string computeLabel() const;

    std::string to_str() const;
    std::string timeRangesStr() const;

    bool operator==(const Subgraph&) const;
    bool operator<(const Subgraph&) const;

    unsigned int support;
    std::list<QDateTime> timeRanges;
    std::string name;
    std::string str;
    std::string label;

private:

    bool contains(const Edge &e, Subgraph &from) const;
    friend std::ostream& operator<< (std::ostream& out, const Subgraph& subg);
    digraph* const s;
};


#endif // SUBGRAPH_H
