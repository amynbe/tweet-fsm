TEMPLATE = app
TARGET = TweetFSM 

QT        += core sql 

QT       -= gui

CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
SOURCES += \
    main.cpp     node.cpp     frequentsubgraphminer.cpp     subgraph.cpp   twgraph.cpp  graphloader.cpp \
    subgraphanalyzer.cpp
#    test.cpp \
#    bglcowtest.cpp
#    VertexProperties.cpp
#    subgraph_properties.cpp
#    testcopyctor.cpp

HEADERS += \
    node.h \
    frequentsubgraphminer.h \
    subgraph.h \
    graphloader.h \
    twgraph.h \
	graph_types.h \
    subgraphanalyzer.h

INCLUDEPATH += $$PWD/../boost_1_51_0 \
DEPENDPATH += $$PWD/../boost_1_51_0 \

LIBS += -fopenmp
