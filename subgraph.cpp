#include "subgraph.h"
#include "node.h"
#include "graph_types.h"
#include <boost/graph/directed_graph.hpp>
#include <iostream>
#include <string>
#include <QDebug>

Subgraph::Subgraph(const digraph* root, const Edge &singleEdge, QDateTime firstTimeRange, string name) :
    support(1), /*rootGraph(root),*/ name(name), s(new digraph())
{
    timeRanges.push_back(firstTimeRange);

    digraph::vertex_descriptor u = boost::source(singleEdge, *root);
    digraph::vertex_descriptor v = boost::target(singleEdge, *root);

    s->add_edge(s->add_vertex((*root)[u]), s->add_vertex((*root)[v]));

    str = to_str();
    label = computeLabel();
}

Subgraph::Subgraph(const Subgraph &s1, const Edge &newEdge, const Subgraph& from, const string name) :
        support(0)/*, rootGraph(s1.root())*/, name(name), s(new digraph())
{
    timeRanges = s1.timeRanges;
    const digraph& orig = *s1.graph();
    digraph::vertex_iterator it, end;

    //add vertices from original graph to this one
    for (boost::tie(it, end) = boost::vertices(orig); it != end; ++it)
    {
        s->add_vertex(orig[*it]);
    }
    const digraph & other = *from.graph();

    //add vertices from other graph's edge to this one
    digraph::vertex_descriptor u = boost::source(newEdge, other);
    digraph::vertex_descriptor v = boost::target(newEdge, other);
    s->add_vertex(other[u]);
    s->add_vertex(other[v]);

    str = to_str();
    label = computeLabel();

}

digraph::vertex_descriptor Subgraph::src(const Edge& e) const {
    return boost::source(e, *s);
}

digraph::vertex_descriptor Subgraph::tgt(const Edge& e) const {
    return boost::target(e, *s);
}

bool Subgraph::contains(const Edge& e, Subgraph& from) const
{
    edge_iterator i1, end1;

    const digraph& g1 = *s;
    const digraph& g2 = *from.graph();

    std::string src2 = srcname(e, g2);
    std::string tgt2 = tgtname(e, g2);

    for (boost::tie(i1, end1) = edges(); i1 != end1; ++i1) {
        const Edge& e1 = *i1;

        std::string src1 = srcname(e1, g1);
        std::string tgt1 = tgtname(e1, g1);


        if ((src1 == src2 && tgt1 == tgt2)
         || (src1 == tgt2 && tgt1 == src2))
        {
            return true;
        }
    }
    return false;
}

/*
 find in s1 an edge that is in the periphery of this->s subgraph
 */
bool Subgraph::findPeripheralEdge(Edge& found, Subgraph &other) const {

    if (this->operator ==(other)) return false;

	using namespace boost;
	edge_iterator i1, end1, i2, end2;

    const digraph& g1 = *s;
    const digraph& g2 = *other.graph();
	
//    std::cout << "num edges=" << boost::num_edges(s)<< std::endl;
    for (boost::tie(i1, end1) = this->edges(); i1 != end1; ++i1) {
        const Edge& e1 = *i1;
        for (boost::tie(i2, end2) = other.edges(); i2 != end2; ++i2) {
            const Edge& e2 = *i2;
			//e1 must have an end node (source or target) in s2 and the other outside
//            std::cout << "Comparing (" << srcname(e1, g1) // get(get(vertex_name_t(), s1.getGraph()), boost::source(e1, s1.getGraph()))
//                      << ","
//                      << tgtname(e1, g1) //get(get(vertex_name_t(), s1.getGraph()), boost::target(e1, s1.getGraph()))
//                      << ") with ("
//                      << srcname(e2, g2) //get(get(vertex_name_t(), s2.getGraph()), boost::source(e2, s2.getGraph()))
//                      << ","
//                      << tgtname(e2, g2) //get(get(vertex_name_t(), s2.getGraph()), boost::target(e2, s2.getGraph()))
//                      << ")" << std::endl;
			
            if (contains(e2, other)) continue;

            std::string src1 = srcname(e1, g1);
            std::string tgt1 = tgtname(e1, g1);
            std::string src2 = srcname(e2, g2);
            std::string tgt2 = tgtname(e2, g2);
			
//			std::string n1 = "(" + src1 + "," + tgt1 + ")";
//			std::string n2 = "(" + src2 + "," + tgt2 + ")";
			
//            std::cout << n1 << ".source=" << src1 << (src(e1) == src(e2) ? " == " : " != ") << n2 << ".source=" << src2 << std::endl;
//            std::cout << n1 << ".target=" << tgt1 << (tgt(e1) == tgt(e2) ? " == " : " != ") << n2 << ".target=" << tgt2 << std::endl;
//            std::cout << n1 << ".source=" << src1 << (src(e1) == tgt(e2) ? " == " : " != ") << n2 << ".target=" << tgt2 << std::endl;
//            std::cout << n1 << ".target=" << tgt1 << (tgt(e1) == src(e2) ? " == " : " != ") << n2 << ".source=" << src2 << std::endl;

            if ((src1 == src2 && tgt1 != tgt2)
			 || (tgt1 == tgt2 && src1 != src2)
			 || (src1 == tgt2 && tgt1 != src2)
			 || (tgt1 == src2 && src1 != tgt2))
			{
                found = e2;
				return true;
			}
		}
	}
    return false;
}

std::pair<edge_iterator, edge_iterator> Subgraph::edges() const {
    return boost::edges(*s);
}

const std::string Subgraph::computeLabel() const{
	using namespace std;
    std::list<std::string> names;
    std::string namesString;

	{
		using namespace boost;
        digraph::vertex_iterator it, end;
        for (boost::tie(it, end) = vertices(*s); it != end; ++it) {
            names.push_back((*s)[*it].name);
        }
	}

	names.sort();
    for (std::list<std::string>::iterator it = names.begin(); it != names.end(); ++it) {
		namesString += (*it);
	}

	return namesString;
}

std::string Subgraph::to_str() const {

//    const digraph & const_s = (const digraph &) *s;
	std::string out;
    std::string spc = "";

    //out += "\n";
    {
	using namespace boost;
	edge_iterator ei, ei_end;
    for (boost::tie(ei, ei_end) = edges(); ei != ei_end; ++ei)
        out += "(" + (*s)[source(*ei, *s)].name + ","
                   + (*s)[target(*ei, *s)].name + ")" + spc;
//        out += "\n";
        spc = " ";
	}
    return out;
}

string Subgraph::timeRangesStr() const
{
    std::stringstream ss;
    for (std::list<QDateTime>::const_iterator it = timeRanges.begin();
         it != timeRanges.end(); ++it)
        ss << "<" <<((QDateTime)*it).toString().toStdString() << ">\n";
    return ss.str();
}

//containsEdge(sub& s, Edge e)
//{
//	i
//}


struct DifferentEdges: public std::binary_function<Edge, Edge, bool> {
    DifferentEdges(const digraph& g1, const digraph& g2) :
			g1(g1), g2(g2) {
	}

    bool operator()(const Edge& e1, const Edge& e2) {
        std::string src1 = srcname(e1, g1);
        std::string src2 = srcname(e2, g2);
        std::string tgt1 = tgtname(e1, g1);
        std::string tgt2 = tgtname(e2, g2);
        return src1 != src2	|| tgt1 != tgt2;
	}
private:
    const digraph& g1, &g2;
};

/** returns an iterator to the first occurrence in the range [first1, last1) of any edge that is NOT in [first2, last2) **/
edge_iterator find_edge_difference(edge_iterator first1, edge_iterator last1,
        edge_iterator first2, edge_iterator last2, const digraph& g1,
        const digraph& g2) {
	return std::find_first_of(first1, last1, first2, last2,
			DifferentEdges(g1, g2));
}

bool Subgraph::operator==(const Subgraph &other) const {
    if (boost::num_edges(*this->s) != boost::num_edges(*other.s))
		return false;
	//return true if each edge of s1 is in s2 and vice versa
	//for this we use set_difference, but edge sets have to be sorted first
	//so we first sort edge sets using an ordering between sets
	//two edges are equal if they have the same source.name and target.name

	edge_iterator this_it, this_end, other_it, other_end;
    boost::tie(this_it, this_end) = boost::edges(*this->s);
    boost::tie(other_it, other_end) = boost::edges(*other.s);

	//
	//std::sort(this_it, this_end, edgeComparator);
	//std::sort(other_it, other_end, edgeComparator);

	//vector<sub::edge_descriptor> diff;

	//std::set_difference(this_it, this_end,
	//					other_it, other_end, 
	//					diff.begin(), edgeComparator);

	//return true if all edges of this.s are found in other.s

//	return this_end
//			== find_edge_difference(this_it, this_end, other_it, other_end,
//					this->s, other.s);


    //compute canonical labeling
    return label == other.label;

}

bool Subgraph::operator<(const Subgraph& other) const {
    digraph::edges_size_type i = boost::num_edges(*this->s);
    digraph::edges_size_type j = boost::num_edges(*other.s);
	if (i != j)
		return i < j;

    //compute canonical labeling
    return label < other.label;

}

std::ostream& operator<<(std::ostream& out, const Subgraph& subg) {
    return out << subg.name //<< ": " << &subg<< "->" << &subg.s
               << subg.to_str() << " x" << subg.support ;
//               << " (" << boost::num_edges(subg.s) << ")";
}
